/* verilator lint_off UNUSED */
/* verilator lint_off UNDRIVEN */
`include "defs.v"

module control(input [2**`TIMEDECW-1:0] timeSig, 
	input [2**`OPRDECW-1:0] oprSig, input [`ADRW-1:0] adrSig, input bufI, 
	bufE, bufS, bufR, bufFGI, bufFGO, bufIEN, bufP, input [`WORDW-1:0] bufDR, 
	bufAC, output reg [`SEL-1:0] selectC, output reg [`OPR-1:0] oprC, 
	output reg MEMweC, ARenC, ARclrC, ARincC, PCenC, PCclrC, PCincC, 
	DRenC, DRclrC, DRincC, ACenC, ACclrC, ACincC, IRenC, TRenC, TRclrC, 
	TRincC, SCclrC, SCincC, EclrC, EcplC, SclrC, RinC, RclrC, FGIclrC, 
	FGOclrC, IENinC);

	/* AND */
	/* ADD */

	always @(*) begin
		if (~bufP) begin
			/* R`.T0: AR <- PC */
			if (~bufR & timeSig[0]) begin
				selectC = 1;
				MEMweC = 0;
				ARenC = 1;
				ARclrC = 0;
				ARincC = 0;
				PCenC = 0;
				PCclrC = 0;
				PCincC = 0;
				ACenC = 0;
				ACclrC = 0;
				ACincC = 0;
				SCclrC = 0;
				SCincC = 1;
				EclrC = 0;
				EcplC = 0;
				SclrC = 0;
				RclrC = 0;
			end
			/* R`.T1: MOW <- M[AR] */
			if (~bufR & timeSig[1]) begin
				
			end
			/* R`.T2: IR <- M[AR], PC <- PC + 1 */
			if (~bufR & timeSig[2]) begin
				selectC = 6;
				PCincC = 1;
				IRenC = 1;
			end
			/* R`.T3: AR <- IR[11:0], I <- IR[15]*/
			if (~bufR & timeSig[3]) begin
				selectC = 4;
				PCincC = 0;
				IRenC = 0;
			end
			/* R`.T4: MOW <- M[AR] */
			if (~bufR & timeSig[4]) begin
				
			end
			/* D7`.I.T5: AR <- M[AR] */
			if (~oprSig[7] & bufI & timeSig[5]) begin
				selectC = 0;
			end
			/* D7`.I.T6: MOW <- M[AR] */
			if (~oprSig[7] & bufI & timeSig[6]) begin
				
			end
			/* (D0|D1).T7: DR <- M[AR] */
			if ((oprSig[0] | oprSig[1]) & timeSig[7]) begin
				selectC = 2;
				DRenC = 1;
				DRclrC = 0;
				DRincC = 0;
			end
			/* D0.T8: AC <- AC^DR, SC <- 0 */
			if (oprSig[0] & timeSig[8]) begin
				oprC = 1;
				ACenC = 1;
				SCclrC = 1;
				SCincC = 0;
			end
			/* D1.T8: AC <- AC + DR, E <- Cout, SC <- 0 */
			if (oprSig[1] & timeSig[8]) begin
				oprC = 0;
				ACenC = 1;
				SCclrC = 1;
				SCincC = 0;
			end
		end
		/* reset all states */
		if (bufP) begin
			MEMweC = 0;
			PCenC = 0;
			PCclrC = 1;
			PCincC = 0;
			SCclrC = 1;
			SCincC = 0;
			RinC = 0;
			RclrC = 1;
			IENinC = 0;
		end

	end
endmodule
