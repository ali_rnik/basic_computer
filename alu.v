/* verilator lint_off UNUSED */
`include "defs.v"
module alu (input [`WORDW-1:0] in1, in2, input [`INOUTW-1:0] in3, 
	input [`OPR-1:0] opr, output reg [`WORDW-1:0] out, output carry);

	wire [`WORDW-1:0] tmp;
	assign {carry, tmp} = {1'b0, in1} + {1'b0, in2};

	always @(*) begin
		case (opr)
			// addition
			`OPR'd0: out = in1 + in2;
			// and
			`OPR'd1: out = in1 & in2;
			// shift left
			`OPR'd2: out = {in1[`WORDW-2:0], in1[`WORDW-1]};
			// shift right
			`OPR'd3: out = {in1[0], in1[`WORDW-1:1]};
			// complement in1
			`OPR'd4: out = ~in1;
			// in1
			`OPR'd5: out = in1;
			// in3
			`OPR'd6: out = {`INOUTW'b0, in3};

			default: out = in1;
		endcase
	end
endmodule
