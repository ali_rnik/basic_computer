`include "defs.v"
module register #(parameter WIDTH = `WORDW) (input [WIDTH-1:0] in, input en, 
	clr, inc, clk, output reg [WIDTH-1:0] out);

	always @(posedge clk or posedge clr) begin
		if (clr)
			out <= 0;
		else if (inc)
			out <= out + 1;
		else if (en) begin
			out <= in;
		end
	end
endmodule
