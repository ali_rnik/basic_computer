`include "defs.v"

module basic_computer (input [`INOUTW-1:0] portINPR, input portFGI, portFGO, 
	portS, clk, portP, output reg [`INOUTW-1:0] portOUTR);

	reg bufE, bufFGI, bufFGO, bufR, bufS, bufIEN, bufI;

	wire carryOutW;
	wire [`ADRW-1:0] arOutW, pcOutW;
	wire [`WORDW-1:0] memOutW, drOutW, acOutW, irOutW, trOutW, busOutW, 
		aluOutW;
	wire [`INOUTW-1:0] inprOutW;
	wire [`SCW-1:0] scOutW;
	wire [2**`TIMEDECW-1:0] tdecOutW;
	wire [2**`OPRDECW-1:0] oprdecOutW;

	wire [`SEL-1:0] selectC;
	wire MEMweC, ARenC, ARclrC, ARincC, PCenC, PCclrC, PCincC, DRenC, 
		DRclrC, DRincC, ACenC, ACclrC, ACincC, IRenC, TRenC, TRclrC, 
		TRincC, SCclrC, SCincC, EclrC, EcplC, SclrC, RinC, RclrC, 
		FGIclrC, FGOclrC, IENinC;
	wire [`OPR-1:0] oprC;

	bus BUS(.mem(memOutW), .ar(arOutW), .pc(pcOutW), .dr(drOutW), 
		.ac(acOutW), .ir(irOutW), .tr(trOutW), .select(selectC), 
		.out(busOutW));

	memory MEM(.in(busOutW), .adr(arOutW), .we(MEMweC), .clk(clk), 
		.out(memOutW));

	register #(.WIDTH(`ADRW)) AR(.in(busOutW[`ADRW-1:0]), .en(ARenC), 
		.clr(ARclrC), .inc(ARincC), .clk(clk), .out(arOutW));

	register #(.WIDTH(`ADRW)) PC(.in(busOutW[`ADRW-1:0]), .en(PCenC), 
		.clr(PCclrC), .inc(PCincC), .clk(clk), .out(pcOutW));

	register #(.WIDTH(`WORDW)) DR(.in(busOutW), .en(DRenC), .clr(DRclrC), 
		.inc(DRincC), .clk(clk), .out(drOutW));

	register #(.WIDTH(`WORDW)) AC(.in(aluOutW), .en(ACenC), .clr(ACclrC), 
		.inc(ACincC), .clk(clk), .out(acOutW));

	register #(.WIDTH(`WORDW)) IR(.in(busOutW), .en(IRenC), .clr(1'b0), 
		.inc(1'b0), .clk(clk), .out(irOutW));

	register #(.WIDTH(`WORDW)) TR(.in(busOutW), .en(TRenC), .clr(TRclrC), 
		.inc(TRincC), .clk(clk), .out(trOutW));

	register #(.WIDTH(`INOUTW)) OUTR(.in(busOutW[`INOUTW-1:0]), .en(1'b1), 
		.clr(1'b0), .inc(1'b0), .clk(clk), .out(portOUTR));

	register #(.WIDTH(`INOUTW)) INPR(.in(portINPR), .en(1'b1), .clr(1'b0), 
		.inc(1'b0), .clk(clk), .out(inprOutW));

	register #(.WIDTH(`SCW)) SC(.in(scOutW), .en(1'b1), .clr(SCclrC), 
		.inc(SCincC), .clk(clk), .out(scOutW));

	alu ALU(.in1(drOutW), .in2(acOutW), .in3(inprOutW), .opr(oprC), 
		.out(aluOutW), .carry(carryOutW));

	register #(.WIDTH(1)) E(.in(carryOutW), .en(1'b1), .clr(EclrC), 
		.inc(EcplC), .clk(clk), .out(bufE));

	register #(.WIDTH(1)) S(.in(portS), .en(1'b1), .clr(SclrC), 
		.inc(1'b0), .clk(clk), .out(bufS));

	register #(.WIDTH(1)) I(.in(irOutW[`WORDW-1]), .en(1'b1), .clr(1'b0), 
		.inc(1'b0), .clk(clk), .out(bufI));

	register #(.WIDTH(1)) R(.in(RinC), .en(1'b1), .clr(RclrC), .inc(1'b0), 
		.clk(clk), .out(bufR));

	register #(.WIDTH(1)) FGI(.in(portFGI), .en(1'b1), .clr(FGIclrC), 
		.inc(1'b0), .clk(clk), .out(bufFGI));

	register #(.WIDTH(1)) FGO(.in(portFGO), .en(1'b1), .clr(FGOclrC), 
		.inc(1'b0), .clk(clk), .out(bufFGO));

	register #(.WIDTH(1)) IEN(.in(IENinC), .en(1'b1), .clr(1'b0), 
		.inc(1'b0), .clk(clk), .out(bufIEN));

	decoder #(.WIDTH(`TIMEDECW)) TDEC(.in(scOutW), .out(tdecOutW));

	decoder #(.WIDTH(`OPRDECW)) 
		OPRDEC(.in(irOutW[`WORDW-2:`WORDW-2-(`OPR-1)]), 
		.out(oprdecOutW));

	control CONTROL(.timeSig(tdecOutW), .oprSig(oprdecOutW), 
		.adrSig(irOutW[`WORDW-2-`OPR:0]), .bufI(bufI), .bufE(bufE), 
		.bufS(bufS), .bufR(bufR), .bufFGI(bufFGI), .bufFGO(bufFGO), 
		.bufIEN(bufIEN), .bufDR(drOutW), .bufAC(acOutW), .bufP(portP), 
		.selectC(selectC), .oprC(oprC), .MEMweC(MEMweC), 
		.ARenC(ARenC), .ARclrC(ARclrC), .ARincC(ARincC), 
		.PCenC(PCenC), .PCclrC(PCclrC), .PCincC(PCincC), 
		.DRenC(DRenC), .DRclrC(DRclrC), .DRincC(DRincC), 
		.ACenC(ACenC), .ACclrC(ACclrC), .ACincC(ACincC), 
		.IRenC(IRenC), .TRenC(TRenC), .TRclrC(TRclrC), 
		.TRincC(TRincC), .SCclrC(SCclrC), .SCincC(SCincC), 
		.EclrC(EclrC), .EcplC(EcplC), .SclrC(SclrC), .RinC(RinC), 
		.RclrC(RclrC), .FGIclrC(FGIclrC), .FGOclrC(FGOclrC), 
		.IENinC(IENinC));
endmodule
