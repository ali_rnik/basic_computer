module decoder #(parameter WIDTH = 3) (input [WIDTH-1:0] in, 
	output [2**WIDTH-1:0] out);

	assign out = {{(2**WIDTH-1){1'b0}}, 1'b1} << in;

endmodule
