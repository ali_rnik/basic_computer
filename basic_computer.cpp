#include <iostream>
#include <unistd.h>
#include "obj_basic_computer/Vbasic_computer.h"
#include "verilated.h"
#include "verilated_vcd_c.h"

using namespace std;

int main(int argc, char **argv)
{
	Verilated::commandArgs(argc, argv);
	Verilated::traceEverOn(true);

	Vbasic_computer *basic_computer = new Vbasic_computer;
	VerilatedVcdC* tfp = new VerilatedVcdC;
	basic_computer->trace(tfp, 99);
	tfp->open("./basic_computer.vcd");

	basic_computer->clk = 0;
	basic_computer->portP = 1;
	for (int i = 0; i <= 40; i++, 
		basic_computer->clk = ~basic_computer->clk) {
		if (i == 2)
			basic_computer->portP = 0;

		basic_computer->eval();

		tfp->dump(i);

		//sleep(1);
	}
	tfp->close();
}
