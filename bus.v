`include "defs.v"
module bus (input [`WORDW-1:0] mem, dr, ac, ir, tr, input [`ADRW-1:0] ar, pc, 
	input [`SEL-1:0] select, output reg [`WORDW-1:0] out);

	always @(*) begin
		case (select)
			`SEL'd0: out = {4'b0, ar};
			`SEL'd1: out = {4'b0, pc};
			`SEL'd2: out = dr;
			`SEL'd3: out = ac;
			`SEL'd4: out = ir;
			`SEL'd5: out = tr;
			`SEL'd6: out = mem;
			default: out = mem;
		endcase
	end

endmodule
