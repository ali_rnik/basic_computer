`include  "defs.v"
module memory (input [`WORDW-1:0] in, input [`ADRW-1:0] adr, input we, clk, 
	output reg [`WORDW-1:0] out);

	reg [`WORDW-1:0] mem[`CELLS];

	initial begin
		mem[0] = 16'h9001;
		mem[1] = 16'd1;
	end

	always @(posedge clk) begin
		if (we)
			mem[adr] <= in;
		out <= mem[adr];
	end
endmodule
